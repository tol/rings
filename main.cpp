#include <iostream>


// #include <casacore/casa/Arrays/ArrayFwd.h>

// #include <casacore/casa/Arrays/Array.h>

#include <casacore/casa/Arrays/Vector.h>

#include<synthesis/MeasurementComponents/Calibrater.h>

int main() {
    // casacore::String msname("/var/scratch/bvdtol/RX42_SB100-109.2ch10s.ms");
    casacore::String msname("/home/vdtol/data/HBA-test.MS");
    
    // std::cout << msname << std::endl;
    // casacore::Table t(msname);

    casacore::MeasurementSet ms(msname);
    casa::Calibrater cb{};

    cb.initialize(ms);


        // mycb.setsolve(type="FRINGE",t=solint,refant=refant,preavg=0.01,
        //               minsnr=minsnr,combine=combine,
        //               zerorates=zerorates,
        //               globalsolve=globalsolve,
        //               niter=niter,
        //               delaywindow=delaywindow,
        //               ratewindow=ratewindow,
        //               paramactive=paramactive,
        //               table=caltable,append=append)



    cb.selectvis();

    // cb.selectvis(
    //     "", // time
	// 	"", // spw
	// 	"", // scan
	// 	"", // field
	// 	"", //	 const casacore::String& intent="",
	// 	"", //	 const casacore::String& obsIDs="",
	// 	"", //	 const casacore::String& baseline="",
	// 	"", //	 const casacore::String& uvrange="",
	// 	"none", //	 const casacore::String& chanmode="none",
	// 	0, // 	 const casacore::Int& nchan=1,
	// 	0, // 	 const casacore::Int& start=0, 
	// 	0 	//  const casacore::Int& step=1,
    // );

    casacore::Vector<casacore::Double> ratewindow=casacore::Vector<casacore::Double>();
    casacore::Vector<casacore::Bool> paramactive=casacore::Vector<casacore::Bool>();
    casacore::Vector<casacore::Double> rmsthresh=casacore::Vector<casacore::Double>();

    cb.setsolve(
        "FRINGE", //type 
        "60s", //solint 
        "caltable", // table
        false, // append 
        0.01, // preavg
        "AP", // apmode 
        4, // minblperant
        "0", // refant
        "flex", // refantmode
        false, // solnorm
        "mean", // normtype
        3.0f, // minsnr
        "", // combine
        0, // fillgaps
        "", // cfcache
        360.0, // painc
        0, // fitorder,
        0.1, // fraction
        -1, // numedge
        "", // radius
        true, // smooth
        false, // zerorates
        true, // globalsolve
        100, // niter
        {-1e6,1e6}, // delaywindow
        {-1e6, 1e6}, // ratewindow
        {true, false, false}, // paramactive
        "" // solmode
    );
    cb.solve();

    std::cout << "Hello, world!" << std::endl;
}

