# Introduction

This repository was created in preparation of making the CASA fringe fitting calibration task available as DPPP step. The purpose is to 

1) find out what is needed to build and link to the casa library 
2) call the fringe fitter from C++
3) identify candidate hooks in the CASA software to provide an alternative source of visibilities, i.e. the DPPP visibility buffer.

# Building CASA

Unfortunately there are no build instructions available for recent CASA releases, so this was done by trial and error.

The casa repository can be found at https://open-bitbucket.nrao.edu/scm/casa/casa6.git

The most recent available tag is 6.2.0.56.

This version does not build against the latest tag of casacore v3.3.0, but it does build against a recent master of casacore (commit 7df1559c)

The casacore repository can found at https://github.com/casacore/casacore.git

The compiler used was gcc 10.2.1

A few modifications were necessary to create a stripped down build on a local machine. This changes have been published in the branch localbuild a fork of CASA at https://git.astron.nl/tol/casa/-/tree/localbuild

# Fringe Fitting Task

The calibration algorithms in CASA share a common interface through the Calibrater class.

The CASA calibration tasks create an instance of this class and initialize it.
The the fringe fitting task is implemented in `casa6/casatasks/src/tasks/task_fringefit.py` which initializes the Calibrater by passing the String "FRINGE" for the 'type' argument to the Calibrater::setsolve(...) function.

Internally this creates an instance of the  FringeJones class, which performs the actual solving.

The solving is initiated by a call to Calibrater::solve(). This in turn calls Calibrater::genericGatherAndSolve(). Inside this function a CalSolVi2Organizer object is created. This object can be configured to use simulated data instead of data from an MS. This seems to already provide some means to use an alternative source of visibilities. This might serve a template for using DPPP
visibilities instead.

The output is written to a CASA caltable. 
For now that is ok, but if the solutions are needed in a subsequent DPPP step
it remains to be investigated how to grab the solutions on time and pass them on.

# Passing the DPPP visibility stream to CASA

There are two candidate points where DPPP can hook on to provide the visibilities to the fringe fitting solver.

1. Adapt Calibrater class
2. Adapt FringeJones class

There does not seem to be much difference in how much detail needs to be considerend for either approaches. The fist approach has the additional advantage that once the fringe fitter step works, it will be fairly straightforward to make more CASA calibration tasks available as DPPP steps.



<!--
3. Adapt CalSolVi2Organizer
2a. Generate and SBDList from the visibilities and call FringeJones::selfSolveOne(SDBList& sdbs)
3b. Adapt FringeJones::selfSolveOne to use the DPPP visibility buffer directly

require that the FringeJones object is properly initialized

-->

